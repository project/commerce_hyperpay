<?php

namespace Drupal\commerce_hyperpay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;

/**
 * Provides the interface for the HyperPay payment gateway.
 */
interface HyperPayInterface extends OffsitePaymentGatewayInterface {

}
