<?php

namespace Drupal\commerce_hyperpay\Transaction\Status;

/**
 * Base class for rejected status due to all kind of validation rules.
 */
abstract class RejectedValidation extends Rejected {

}
