<?php

namespace Drupal\commerce_hyperpay\Transaction\Status;

/**
 * Base class for rejected status due to all kind of risk checks.
 */
abstract class RejectedRisk extends Rejected {

}
